const express = require('express');
var router = express.Router();
const db = require('../models');
const Users = db.Users;
const Messages = db.Messages;
const Connections = require('../WsConnections');
const Sequelize = require("sequelize");

router.get('/users', async (req, res) => {
  try{
    const users = await Users.scope('minimal').findAll();
    const respData = [];
    users.forEach(user => {
      let usr = user.toJSON();
      usr.online = Connections.isUserOnline(user.id);
      //remove current user from list
      if(req.user.id != usr.id) {
        respData.push(usr);
      }
    });
    res.json(respData);
  } catch (err) {
    console.error(err);
    res.status(400).end();
  }
});

router.get('/users/current', async (req, res) => {
  let usr  = await Users.scope('minimal').findByPk(req.user.id);
  usr = usr.toJSON();
  usr.unread = await Messages.findAll({
    where: {
      destination: usr.id,
      status: 0
    },
    attributes: [
      [Sequelize.fn('DISTINCT', Sequelize.col('author')) ,'author'],
    ],
  });
  usr.unread = usr.unread.map(el => el.author);
  res.json(usr);
});

router.post('/users/:authorId/read', async (req, res) => {
  console.log('/read');
  try{
    const result = await Messages.update(
      {status: true},
      {
        where: { 
          status: false,
          destination: req.user.id,
          author: req.params.authorId
        }
      }
    );
    console.log(result);
    res.status(200).end();
  } catch (err) {
    console.error(err);
    res.status(400).end();
  }
});

module.exports = router;
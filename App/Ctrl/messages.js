const express = require('express');
var router = express.Router();
const db = require('../models');
const Messages = db.Messages;
const Sequelize = require("sequelize");
const Op = Sequelize.Op

router.get('/messages/history/:contactId', async (req, res) => {
  const contact = parseInt(req.params.contactId);
  const where = Sequelize.or(
    Sequelize.and(
      { author: contact },
      { destination: req.user.id }
    ),
    Sequelize.and(
      { destination: contact },
      { author: req.user.id }
    )
  );
  try{
    const messagesCount = await Messages.count({where});
    const limit = 20;
    let offset = messagesCount - limit;
    if(offset<0) {
      offset = 0;
    }
    const messages = await Messages.findAll({where,offset,limit});
    res.json(messages);
  } catch(err) {
    console.error(err);
    res.status(500).end();
  }
});

router.get('/messages/history/:contactId/:messId', async (req, res) => {
  const contact = parseInt(req.params.contactId);
  const messId = parseInt(req.params.messId);
  const where = Sequelize.and(
    {id: {[Op.lt]: messId,}},
    Sequelize.or(
      Sequelize.and(
        { author: contact },
        { destination: req.user.id }
      ),
      Sequelize.and(
        { destination: contact },
        { author: req.user.id }
      )
    )
  );
  try{
    const messagesCount = await Messages.count({where});
    const limit = 20;
    let offset = messagesCount - limit;
    if(offset<0) {
      offset = 0;
    }
    const messages = await Messages.findAll({where,offset,limit});
    res.json(messages);
  } catch(err) {
    console.error(err);
    res.status(500).end();
  }
});

module.exports = router;
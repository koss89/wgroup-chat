const express = require('express');
var router = express.Router();
const db = require('../models');
const Users = db.Users;
const crypt = require('../CryptUtil');

router.post('/register', async (req, res) => {
  try{
    const data = req.body;
    data.password = crypt.crypt(data.password);
    const user = await Users.create(data);
    res.json(user.toJSON());
  } catch (err) {
    console.error(err);
    res.status(400).end();
  }
});

module.exports = router;
const express = require('express');
var router = express.Router();
const db = require('../models');
const Users = db.Users;
const crypt = require('../CryptUtil');
const jsonwebtoken = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;

router.post('/auth', async (req, res) => {
  try{
    const user = await Users.scope('withPassword').findOne({ where: { email: req.body.email } });
    if(user == null) {
      throw Error('User Not Found!!');
    } else if(crypt.isSame(req.body.password, user.password)){
      const token = jsonwebtoken.sign(user.toJSON(), secret);
      res.send(token);
    } else {
      throw Error('Wrong Password');
    }
   
  } catch (err) {
    console.error(err);
    res.status(401).json({message: err.message});
  }
});

module.exports = router;
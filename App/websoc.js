const ws = require("ws");
const url = require("url");
const jsonwebtoken = require('jsonwebtoken');
const secret = process.env.JWT_SECRET;

const connections = require('./WsConnections');

const wss = new ws.Server({ port: process.env.WS_PORT });

wss.on("connection", (wsc, request) => {
  console.log("New connection!");
  const q = url.parse(request.url, true);
  const token = q.query["token"];
  let decoded;
  if (token == undefined) {
    wsc.close(4000, "User Not set");
    return;
  } else {
    decoded = jsonwebtoken.verify(token, secret);
    if (!decoded) {
      wsc.close(4000, "Wrong token");
      return;
    }
  }

  wsc.user = decoded;
  connections.addConnection(wsc);

  wsc.on("message", (rawMessage) => {
    let message = JSON.parse(rawMessage);
    message.author = decoded.id;
    console.log(JSON.stringify(message));
    connections.storeAndSendToClient(message);
  });

  wsc.on("close", () => {
    console.log("connect close");
    connections.removeConnection(wsc);
  });

});

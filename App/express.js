const express = require('express');
const app = express();
const router = express.Router();
const bodyParser  = require('body-parser');
const jwt = require('express-jwt');
const secret = process.env.JWT_SECRET;

const registerCtrl = require('./Ctrl/register');
const loginCtrl = require('./Ctrl/login');
const usersCtrl = require('./Ctrl/users');
const messagesCtrl = require('./Ctrl/messages');

const port = process.env.PORT;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static('public'));


router.use(registerCtrl);
router.use(loginCtrl);

router.use(jwt({ secret: secret, algorithms: ['HS256']}).unless({path: ['/register','/auth']}));
router.use(usersCtrl);
router.use(messagesCtrl);


app.use(process.env.BASE_URL, router);

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`);
})

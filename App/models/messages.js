
const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  const Messages = sequelize.define("messages", {
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    author: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    destination: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    message: {
      type: Sequelize.STRING,
      allowNull: false
    },
    status : {
      type: Sequelize.BOOLEAN
    },
    created: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    },
  });
  Messages.associate = function(models) {
    Messages.belongsTo(models.Users, {foreignKey: 'author', as: "authorObj"})
  };

  return Messages;
};
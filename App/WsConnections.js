const db = require('./models');
const Messages = db.Messages;

class WsConnections {
  
  constructor() {
    this.clients = {};

  }

  isUserOnline(cid) {
    return this.clients[cid]!=undefined;
  }

  addConnection(wsc) {
    const cid = wsc.user.id;
    if(this.clients[cid]!=undefined) {
      this.clients[cid].push(wsc);
    } else {
      const clientConnections = [];
      clientConnections.push(wsc);
      this.clients[cid]=clientConnections;
    }
    let usr = wsc.user;
    delete usr.password;
    this.broadcast({
      type: 'contact-status',
      status: 1,
      user: usr
    });
  }

  removeConnection(wsc) {
    const cid = wsc.user.id;
    let clientConnections = this.clients[cid];
    const index = clientConnections.indexOf(wsc);
    if(index!=-1) {
      clientConnections.splice(index, 1);
    }
    if(clientConnections.length==0) {
      let usr = wsc.user;
      this.broadcast({
        type: 'contact-status',
        status: 0,
        user: usr
      });
    }
  }

  storeAndSendToClient(message) {
    Messages.create(message).then(mess => {
      this.sendToClient(mess);
    });
  }

  sendToClient(message) {
    let clientConnections = this.clients[message.destination];
    let authorConnections = this.clients[message.author];
    if(clientConnections) {
      this.sendToConnections(clientConnections, message);
    }
    if(authorConnections) {
      this.sendToConnections(authorConnections, message);
    }
  }

  sendToConnections(connections, message) {
    connections.forEach(connection => {
      connection.send(JSON.stringify(message));
    });
  }

  broadcast(message) {
    Object.keys(this.clients).forEach(key => {
      this.sendToConnections(this.clients[key], message);
    });
  }
}

module.exports = new WsConnections();
const bcrypt = require('bcrypt');
const salt = Number.parseInt(process.env.SALT, 3);

class CryptUtil {

  constructor() {
  }

  crypt(password) {
    return bcrypt.hashSync(password, salt);
  }

  isSame(password, hash) {
    return bcrypt.compareSync(password, hash);
  }
}

module.exports = new CryptUtil();
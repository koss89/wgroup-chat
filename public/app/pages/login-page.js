const login = Vue.component('login', {
    template: `
    <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Email:</label>
          <input type="text" class="form-control" v-model="item.email">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Password:</label>
          <input type="password" class="form-control" v-model="item.password">
        </div>
        <button @click="login" class="btn btn-primary">Login</button>
        <router-link :to="'/register'">Register</router-link>
      </div>
      <div class="col-md-3">
      </div>
    </div>
    `,
    data: function () {
      return {
        item: {},
      }
    },
    created: function() {
      Auth.logout();
    },
    methods: {
      login: function () {
        api = new ApiLogin();
        api.login(this.item).then(data => {
          chatWS.connect(Auth.getToken());
          this.$router.push('/');
        });
      },
      
    }
  })

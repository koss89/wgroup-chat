const register = Vue.component('register', {
  template: `
    <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>User Name:</label>
          <input type="text" class="form-control" v-model="item.username">
        </div>
        <div class="form-group">
          <label>Email:</label>
          <input type="email" class="form-control" v-model="item.email">
        </div>
        <div class="form-group">
          <label>Password:</label>
          <input type="password" class="form-control" v-model="item.password">
        </div>
        <div class="form-group">
          <label>Re password:</label>
          <input type="password" class="form-control" v-model="item.repassword">
          <small class="form-text text-danger" v-if="item.password!==item.repassword">Password not same.</small>
        </div>
        <button @click="Register" :disabled="item.password!==item.repassword" class="btn btn-primary">Register</button>
      </div>
      <div class="col-md-3">
      </div>
    </div>
    `,
  data: function () {
    return {
      item: {},
    }
  },
  created: function () {
    Auth.logout();
  },
  methods: {
    Register: function () {
      if(this.item.password===this.item.repassword) {
        api = new ApiRegister();
        api.register(this.item).then(data => {
          this.$router.push('/login');
        });
      }
      
    },

  }
})

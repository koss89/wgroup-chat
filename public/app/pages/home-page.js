const home = Vue.component('home', {
    template: `
    <div class="home-wrap">
      <socket-status />
      <div class="row row-home-wrap">
        <div class="col-3">
          <users-list v-model="selected"/>
        </div>
        <div class="col-9">
          <chat-history :contact="selected"></chat-history>
          <chat-message v-if="selected.id && chatWS.isOnline()" @onSend="sendMessage"/>
        </div>
      </div>
    </div>
      `,
      data: function () {
        return {
          selected: {},
          chatWS: chatWS
        }
      },
      methods: {
        sendMessage: function(text) {
          const message = {
            destination : this.selected.id,
            message: text
          }
          chatWS.send(JSON.stringify(message));
        }
      },
  })
  
class auth {

  Token;

  localStorageTokenItemName = 'Token';

  constructor() {
    let tmpToken = localStorage.getItem(this.localStorageTokenItemName);
    if (tmpToken) {
      this.Token = tmpToken;
    }
  }

  getToken() {
    return this.Token;
  }

  setToken(token) {
    this.Token = token;
    localStorage.setItem(this.localStorageTokenItemName, token);
  }

  logout() {
    this.Token = undefined;
    localStorage.removeItem(this.localStorageTokenItemName);
  }

  isAuth() {
    return this.Token ? true : false;
  }
}

const Auth = new auth();

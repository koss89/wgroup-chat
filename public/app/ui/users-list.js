const usersList = Vue.component('usersList', {
    template: `
    <div>
      <ul class="list-group">
        <li class="list-group-item" v-bind:class="{ 'active' : item.id==selected.id}" v-for="item in items" :key="item.id" @click="select(item)">
          <span class="badge badge-pill" :class="{'badge-success' : item.online, 'badge-secondary' : !item.online}">&nbsp;</span>
          <span>{{item.username}}</span>
          <span class="badge badge-pill badge-info" v-if="item.unread">&nbsp;</span>
        </li>
      </ul>
    </div>
    `,
    props: {
      value: {}
    },
    data: function () {
      return {
        selected: {}
      }
    },
    computed: {
      items: function() {
        return Contacts.getList();
      }
      
    },
    created: function() {
      const api = new ApiUsers();
      api.getAll();
    },
    methods: {
      select: function(item) {
        this.$emit('input', item);
        this.selected=item;
        Messages.selected = item;
        const api = new ApiUsers();
        if(item.unread) {
          api.setRead(item.id).then(() => {
            Contacts.setUnread(item.id, false);
          });
        }
      }
    }
  })

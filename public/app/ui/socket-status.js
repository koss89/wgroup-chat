const socketStatus = Vue.component('socketStatus', {
  template: `
  <div :class="{'bg-warning': !chatWS.isOnline()}">
    <div class="row">
      <div class="col">
        Connection: 
        <span class="text-success" v-if="chatWS.isOnline()">Online</span>
        <span class="text-danger" v-if="!chatWS.isOnline()">Offline</span>
      </div>
      <div class="col-1">
        <button class="btn btn-primary" v-if="!chatWS.isOnline()" @click="connect()">Connect</button>
      </div>
    </div>
  </div>
  `,
  data: function () {
    return {
      chatWS: chatWS
    }
  },
  methods: {
    connect: function() {
      chatWS.connect(Auth.getToken());
    }
  }
})

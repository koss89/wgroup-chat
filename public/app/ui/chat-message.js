const chatMessage = Vue.component('chatMessage', {
  template: `
  <div class="chat-message-wrap">
    <div class="row">
      <div class="col">
        <textarea class="form-control" v-model="messageText"/>
      </div>
      <div class="col-1">
        <button class="btn btn-primary" @click="send()">Send</button>
      </div>
    </div>
  </div>
  `,
  data: function () {
    return {
      messageText: '',
    }
  },
  methods: {
    send: function() {
      this.$emit('onSend', this.messageText);
      this.messageText = '';
    }
  }
})

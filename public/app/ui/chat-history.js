const chatHistory = Vue.component('chatHistory', {
  template: `
  <div ref="chatHistoryRef" v-if="contact" class="chat-history-wrap">
    <chat-history-message v-for="item in history" :key="item.id" :message="item"></chat-history-message>
  </div>
  `,
  props: {
    contact: undefined
  },
  data() {
    return {
      isScroll: true
    }
  },
  updated: function() {
    if(this.$refs.chatHistoryRef && this.isScroll) {
      this.$refs.chatHistoryRef.scrollTop = 4000000;
    }
  },
  mounted: function() {
    this.$refs.chatHistoryRef.addEventListener('scroll', function() {
      if(this.$refs.chatHistoryRef.scrollTop==0 && Messages.getList()[this.contact.id][0] && Messages.getList()[this.contact.id][0].id) {
        api = new ApiMessages();
          api.getHistory(this.contact.id, Messages.getList()[this.contact.id][0].id).then( data => {
            this.isScroll = false;
            data.forEach(mess => {
              Messages.addMessage(mess);
            });
            this.$refs.chatHistoryRef.scrollTop = 5;
            setTimeout(function () {
              this.isScroll = true;
            }, 1000);
          });
      }
    }.bind(this));
  },
  computed: {
    history: function() {
      return Messages.getList()[this.contact.id];
    }
  },
  watch: {
    contact: function(oldVal, newVal) {
      if(oldVal!==newVal) {
        if(!Contacts.getContact(this.contact.id).history) {
          api = new ApiMessages();
          api.getHistory(this.contact.id).then( data => {
            Contacts.getContact(this.contact.id).history = true;
            data.forEach(mess => {
              Messages.addMessage(mess);
            });
          });
        }
      }
    }
  },
})

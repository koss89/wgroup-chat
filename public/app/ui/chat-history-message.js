const chatHistoryMessage = Vue.component('chatHistoryMessage', {
  template: `
  <div class="chat-history-message-wrap" :class="{ 'text-right border-right' : !message.inc, 'border-left' : message.inc}">
    <div class="text-muted small">
      {{new Date(message.created).toLocaleString()}}
    </div>
    <div >
      {{message.message}}
    </div>
  </div>
  `,
  props: {
    message: undefined
  },
})

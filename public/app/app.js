const router = new VueRouter({
    routes : [
      { path: '/', component: home },
      { path: '/login', component: login },
      { path: '/register', component: register },
    ]
  });
  
  Vue.use(Toasted);
  
  const app = new Vue({
    router
  }).$mount('#app');
  
  
  
class API {

  basePath = '/api/';

  getM(endpoint, auth=false) {
    self = this;
    return new Promise(function (resolve, reject) {
      axios.get(`${self.basePath}${endpoint}`, self.getAuthHeaders(auth)).then(function (resp) {
        resolve(resp.data);
      }).catch(function (err) {
        self.handleError(err);
        reject(err);
      })
    });
  }

  post(endpoint, data, auth=false) {
    self = this;
    return new Promise(function (resolve, reject) {
      axios.post(`${self.basePath}${endpoint}`, data, self.getAuthHeaders(auth)).then(function (resp) {
        resolve(resp.data);
      }).catch(function (err) {
        self.handleError(err);
        reject(err);
      })
    });
  }

  getAuthHeaders(auth) {
    let headers ={};
    if(auth==true) {
      headers.Authorization = 'Bearer '+Auth.getToken();
    }
    return { headers };
  }

  handleError(err) {
    Vue.toasted.error('Error!').goAway(3000);
    if (err.response.status == 401) {
      router.push('/login');
    }
    console.error(err);
  }
}

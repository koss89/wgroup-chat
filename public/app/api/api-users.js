let currentUser;
class ApiUsers {

  Api;
  endpoint = 'users';

  constructor() {
    this.Api = new API();
    if(!currentUser) {
      this.getCurrent();
    }
  }

  getAll() {
    return this.Api.getM(this.endpoint, true).then(resp => {
      Contacts.setList(resp);
      currentUser.unread.forEach(el => {
        Contacts.setUnread(el, true);
      });
    });
  }

  getCurrent() {
    return this.Api.getM(`${this.endpoint}/current`, true).then(resp => {
      currentUser = resp;
    });
  }

  setRead(contactId) {
    return this.Api.post(`${this.endpoint}/${contactId}/read`, {}, true);
  }
}

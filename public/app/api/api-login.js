class ApiLogin {

  Api;
  endpoint = 'auth';

  constructor() {
    this.Api = new API();
  }
  login(item) {
    return this.Api.post(this.endpoint, item).then(resp => {
      Auth.setToken(resp);
    });
  }
}

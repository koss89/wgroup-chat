class ApiMessages {

  Api;
  endpoint = 'messages';

  constructor() {
    this.Api = new API();
  }

  getHistory(contactId, fromId = null) {
    if(fromId) {
      return this.Api.getM(`${this.endpoint}/history/${contactId}/${fromId}`, true);
    }
    return this.Api.getM(`${this.endpoint}/history/${contactId}`, true);
  }
  
}

class ApiRegister {

  Api;
  endpoint = 'register';

  constructor() {
    this.Api = new API();
  }

  register(item) {
    return this.Api.post(this.endpoint, item);
  }
  
}

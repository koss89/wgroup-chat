const Messages = new Vue({
  data() {
    return {
      messagesList: {},
      selected: {}
    }
  },
  methods: {
    addMessage(message) {
      let id = message.author;
      message.inc = true;
      if(id === currentUser.id) {
        id = message.destination;
        message.inc = false;
      }
      if(message.inc && this.selected.id!==message.author) {
        Contacts.setUnread(message.author, true);
      }
      
      if(this.messagesList[id]==undefined) {
        this.messagesList[id] = [];
      }
      this.messagesList[id].splice(message, 0, message);
      this.messagesList[id].sort((a, b) => {
        let resutl = 0;
        if(a.id<b.id) {
          resutl = -1;
        }
        if(a.id>b.id) {
          resutl = 1;
        }
        return resutl;
      });
      this.messagesList = Object.assign({}, this.messagesList);
    },
    getList() {
      return this.messagesList;
    }
  }
});

const Contacts = new Vue({
  data() {
    return {
      contactList: {},
    }
  },
  methods: {
    setList: function(contacts) {
      this.contactList = {};
      contacts.forEach(contact => {
        this.addContact(contact);
      });
    },
    addContact: function(contact) {
      contact.unread = false;
      this.contactList[contact.id] = contact;
      this.refresh();
    },
    getContact: function(id) {
      return this.contactList[id];
    },
    setOnline: function(contact) {
      contact.online = 1;
      this.addContact(contact);
    },
    setOffline: function(contact) {
      contact.online = 0;
      this.addContact(contact);
    },
    setStatus: function(status, contact) {
      if(contact.id!==currentUser.id) {
        contact.online = status ? 1 : 0;
        this.addContact(contact);
      }
    },
    setUnread(id, unread) {
      this.getContact(id).unread = unread;
      this.refresh();
    },
    getList: function() {
      return this.contactList;
    },
    refresh() {
      this.contactList = Object.assign({}, this.contactList);
    }
  }
});
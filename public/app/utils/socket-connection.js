const chatWS = new Vue({
  data() {
    return {
      ws: false,
      online: false,
    }
  },
  created() {
    this.connect(Auth.getToken());
  },
  methods: {
    isOnline: function () {
      return this.online;
    },
    getConnection: function () {
      return this.ws;
    },
    connect: function (token) {
      if (this.isOnline()) return false;
      let url = 'ws://localhost:5000';
      if (token) {
        url += `?token=${token}`;
      }
      this.ws = new WebSocket(url);
      this.ws.addEventListener('open', () => {
        this.online = true;
      });
      this.ws.addEventListener('message', (rawMessage) => {
        let message = JSON.parse(rawMessage.data);
        switch(message.type) {
          case 'contact-status':
              Contacts.setStatus(message.status, message.user);
              break;
          default: Messages.addMessage(message);
        }
      });
      this.ws.addEventListener('close', (data) => {
        console.log('Connection close');
        this.online = false;
      });
      this.ws.addEventListener('error', (err) => { console.error(err) });
    },
    send: function (message) {
      if (this.isOnline()) {
        this.ws.send(message);
      }
    }
  }
})

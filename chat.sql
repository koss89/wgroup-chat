-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 02 2020 г., 11:32
-- Версия сервера: 5.7.29-0ubuntu0.18.04.1
-- Версия PHP: 7.1.33-14+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `chat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) NOT NULL,
  `author` bigint(20) NOT NULL,
  `destination` bigint(20) NOT NULL,
  `message` varchar(5000) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `author`, `destination`, `message`, `status`, `created`, `parent`) VALUES
(1, 1, 2, 'sdfsd', 0, '2020-09-01 23:27:02', NULL),
(2, 1, 2, 'ds', 0, '2020-09-01 23:27:04', NULL),
(3, 1, 2, 'd', 0, '2020-09-01 23:27:05', NULL),
(4, 1, 2, 'd', 0, '2020-09-01 23:27:06', NULL),
(5, 2, 1, 'sdfs', 0, '2020-09-01 23:27:08', NULL),
(6, 2, 1, 'sdfs', 0, '2020-09-01 23:27:14', NULL),
(7, 2, 1, 'dsfsd', 0, '2020-09-01 23:27:15', NULL),
(8, 1, 2, 'd', 0, '2020-09-01 23:27:19', NULL),
(9, 2, 1, 'wqwed', 0, '2020-09-01 23:27:22', NULL),
(10, 2, 3, 'sdas', 0, '2020-09-01 23:28:39', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(500) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created`) VALUES
(1, 'GOD', 'god@god.com', '$2b$10$luSQdXl8sJ1R7Aeoji1T0u63bPhO/Svs400boRtNzr248My1.ZzmW', '2020-09-01 17:52:13'),
(2, 'admin', 'admin@admin.com', '$2b$10$FTDn0gPMtnZwuVCXjP3MAejlsdW9aaHSA/nqPZ/dlAxU9ERsGgrKm', '2020-09-01 17:52:40'),
(3, 'user', 'user@user.com', '$2b$10$PMMRkyfnPaWE/ffBtZ0yKeai/W47KiwAwlFxtx/7YuyAbTPCCqL2O', '2020-09-01 23:28:05');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
